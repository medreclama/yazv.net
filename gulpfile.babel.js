require('./tasks/stylus');
require('./tasks/copy');
require('./tasks/script');
require('./tasks/server');
require('./tasks/template');
require('./tasks/watcher');
require('./tasks/default');

export const projectName = process.env.npm_config_project_name || '';
export const isPages = process.env.NODE_ENV === 'pages';
export const isDebug = (process.env.NODE_ENV !== 'production' && !isPages);

export const distDir = !isPages ? './dist' : './public';
export const templateDir = `${distDir}/template/`;
