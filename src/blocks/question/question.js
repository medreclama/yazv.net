const question = () => {
  const questions = Array.from(document.querySelectorAll('.question'));
  questions.forEach((q) => {
    const head = q.querySelector('.question__header');
    const body = q.querySelector('.question__body');
    head.addEventListener('click', () => {
      q.style.setProperty('--question-body-height', `${body.scrollHeight}px`);
      q.classList.toggle('question--active');
    });
  });
};

export default question;
