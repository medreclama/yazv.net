import Splide from '@splidejs/splide';
import fslightbox from 'fslightbox'; // eslint-disable-line no-unused-vars
import flatpickr from 'flatpickr';
import { Russian } from 'flatpickr/dist/l10n/ru';
import sh from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import headerMobileSwitcher from '../blocks/header-mobile-switcher/header-mobile-switcher';
import sliderWorks from '../blocks/slider-works/slider-works';
import formInput from '../blocks/form-input/form-input';
import appointmentForm from '../blocks/appointment-form/appointment-form';
import question from '../blocks/question/question';
import header from '../blocks/header/header';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import Form from '../blocks/form/form';

sh();
tabs();
headerMobileSwitcher();
sliderWorks();
formInput();
appointmentForm();
question();
header();
headerNavigation();

const licenses = {
  type: 'name',
  required: 'Согласитесь на обработку персональных данных',
};

const formsCreate = () => {
  const validation = {
    methods: {
      tel: {
        rules: [
          (i) => {
            const message = 'Введите правильный номер телефона без восьмёрки';
            return i.value.length !== '+7 (000) 000-00-00'.length ? message : '';
          },
        ],
        type: 'type',
      },
      email: {
        rules: [
          (i) => {
            const message = 'Введите правильный e-mail-адрес';
            return !i.value.toLowerCase().match(/^[a-z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-z0-9.-]+$/) ? message : '';
          },
        ],
        type: 'type',
      },
      name: {
        rules: [
          (i) => {
            const message = 'Минимум три символа';
            return i.value.length > 2 ? '' : message;
          },
          (i) => {
            const message = 'Только символы русского алфавита';
            return i.value.match(/[аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ]+/ig) ? '' : message;
          },
        ],
        type: 'name',
      },
      licenses,
      licenses_popup: licenses,
    },
  };

  const questionForm = document.querySelectorAll('form.new_question');
  const newAppointmentForm = document.querySelectorAll('form.new-appointment-form');
  const message = {
    success: {
      title: 'Заявка принята',
      text: 'Спасибо! Ваше сообщение отправлено!',
    },
    error: {
      title: 'Произошла ошибка',
      text: 'Пожалуйста, попробуйте позже, или запишитесь на приём по телефону +7  (812) 660-49-63',
    },
  };

  if (questionForm) {
    const handler = '/bitrix/templates/tpl_yazv/ajax/ajax.php?model=NewQuestionModel';
    Array.from(questionForm).forEach((form) => {
      new Form({
        form,
        message,
        handler,
      }, validation).init();
    });
  }

  if (newAppointmentForm) {
    const handler = '/bitrix/templates/tpl_yazv/ajax/ajax.php?model=NewAppointmentModel';
    Array.from(newAppointmentForm).forEach((form) => {
      new Form({
        form,
        message,
        handler,
      }, validation).init();
    });
  }
};
formsCreate();
flatpickr('#contacts-appointment-datetime', {
  locale: Russian,
  enableTime: true,
  dateFormat: 'Y-m-d H:i',
  minDate: Date.now(),
});

const fancybox = Array.from(document.querySelectorAll('.fancybox'));
fancybox.forEach((item, i) => {
  item.dataset.fslightbox = item.dataset.fslightbox || `${i}`; // eslint-disable-line
});
refreshFsLightbox(); // eslint-disable-line no-undef

const sliders = Array.from(document.querySelectorAll('.splide'));
const slidersExclude = ['slider-works'];
sliders.forEach((slider) => {
  if (!slider.classList.contains(...slidersExclude)) {
    new Splide(slider, {
      type: 'loop',
    }).mount();
  }
});
const slidersWork = Array.from(document.querySelectorAll('.slider-works'));
slidersWork.forEach((slider) => {
  new Splide(slider, {
    type: 'loop',
    autoplay: true,
    interval: 5000,
    // pagination: false,
  }).mount();
});
